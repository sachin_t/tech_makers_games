import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tech_makers/dashboard_controller.dart';
import 'package:tech_makers/widgets.dart';
import 'package:vimeo_video_player/vimeo_video_player.dart';

class DashBoardView extends StatelessWidget {
  const DashBoardView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      backgroundColor: Colors.black,
      appBar: appBar(),
      body: GetBuilder<DashBoardController>(builder: (controller) {
        return SizedBox(
          height: height,
          width: width,
          child: ListView(
            children: [
              Column(
                children: [
                  sliderWidget(),
                  languageWidget(),
                  SizedBox(height: height * 0.14),
                  timerWidget(),
                  SizedBox(height: height * 0.04),
                  userList(),
                  SizedBox(height: height * 0.04),
                  const VimeoVideoPlayer(
                    url: 'https://vimeo.com/759401631',
                  ),
                  SizedBox(height: height * 0.04),
                  gamesWidget()
                ],
              )
            ],
          ),
        );
      }),
          bottomNavigationBar: bottomNav(),
    ));
  }


}
