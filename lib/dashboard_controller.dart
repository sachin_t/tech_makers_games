import 'package:get/get.dart';
import 'package:flutter/material.dart';

class DashBoardController extends GetxController {

  RxInt languageIndex = 0.obs;

  RxBool isMoreClicked = false.obs;
  RxBool isRunning = false.obs;
  int initialValue = 98498;

  RxInt sliderIndex = 0.obs;
  List<Image> sliderList = [
    Image.asset('assets/images/slider_1.jpg', width: Get.width, fit: BoxFit.fitWidth),
    Image.asset('assets/images/slider_2.jpg', width: Get.width, fit: BoxFit.fitWidth),
    Image.asset('assets/images/slider_3.jpg', width: Get.width, fit: BoxFit.fitWidth),
    Image.asset('assets/images/slider_4.jpg', width: Get.width, fit: BoxFit.fitWidth),
  ];
  List<String> languageList = [
    'Bengali','Konkani','Malayalam', 'Marathi',
    'Nepali','Odia','Punjabi','Tamil','Telugu','Urdu'
  ];

  List<String> userList = [
    'Arjun ₹2700','Eva ₹4280','Ramya ₹4535', 'Ravi ₹1535' , 'Usha ₹2535',
    'Tijo ₹1535','Sachin ₹4535','Usha ₹2535','Eva ₹4280','Ravi ₹1535'
  ];

  List<String> gamesList = [
    'assets/images/ccrush.png','assets/images/subway.png','assets/images/sudoku.png',
    'assets/images/fortine.png' , 'assets/images/pubg.png', 'assets/images/cricket.png'
  ];

  Stream<int> counter() async* {
    while (isRunning.value) {
      await Future.delayed(const Duration(seconds: 2));
      initialValue++;
      yield initialValue;
    }
    update();
  }

  Stream<List<String>> userShuffle() async* {
    while (isRunning.value) {
      await Future.delayed(const Duration(seconds: 3));
      userList.shuffle();
      yield userList;
    }
    update();
  }

  @override
  void onClose() {
    // TODO: implement onClose
    super.onClose();
    isRunning.value = false;
  }

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    isRunning.value = true;
    counter();
  }
}