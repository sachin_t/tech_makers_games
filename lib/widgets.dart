
import 'dart:ui';

import 'package:carousel_indicator/carousel_indicator.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'dashboard_controller.dart';


final width = Get.width;
final height = Get.height;
const Color appColor = Color(0xffDAA520);
LinearGradient gradient = LinearGradient(colors: [
  Colors.purple.shade800,
  Colors.purple.shade500,
]);

PreferredSizeWidget? appBar() {
  return AppBar(
    backgroundColor: Colors.black,
    flexibleSpace: Padding(
      padding: EdgeInsets.only(left: width * 0.03),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Image.asset(
            'assets/images/logo.jpg',
            fit: BoxFit.cover,
            height: height * 0.06,
            width: width * 0.3,
          ),
          const Spacer(),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(
                'assets/images/gift.png',
                fit: BoxFit.contain,
                height: height * 0.04,
              ),
              const Text('Promotions',
                  style: TextStyle(
                      fontSize: 12,
                      color: appColor,
                      fontWeight: FontWeight.w500))
            ],
          ),
          const SizedBox(width: 8),
          Container(
            color: appColor,
            height: double.infinity,
            width: width * 0.21,
            child: const Center(
              child: Text(
                'LOGIN',
                style: TextStyle(color: Colors.white, fontSize: 15),
              ),
            ),
          )
        ],
      ),
    ),
    toolbarHeight: height * 0.08,
  );
}

Widget? bottomNav() {
  return Container(
    height: height * 0.1,
    color: Colors.black,
    child: Stack(
      clipBehavior: Clip.none,
      alignment: Alignment.topCenter,
      children: [
        Align(
          alignment: Alignment.center,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                color: appColor,
                child: const Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.volume_up_outlined,
                      color: Colors.white,
                      size: 35,
                    ),
                    Text('Support',style: TextStyle(color: Colors.white),)
                  ],
                ),
              ),
              const Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.sports_basketball_outlined,
                    color: appColor,
                    size: 35,
                  ),
                  Text('E-Sports',style: TextStyle(color: Colors.white),)
                ],
              ),
              const Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.sports_cricket_outlined,
                    color: appColor,
                    size: 35,
                  ),
                  Text('Cricket',style: TextStyle(color: Colors.white),)
                ],
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                color: appColor,
                child: const Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.person_2_outlined,
                      color: Colors.white,
                      size: 35,
                    ),
                    Text('Register',style: TextStyle(color: Colors.white),)
                  ],
                ),
              ),
            ],
          ),
        ),
        Positioned(
          top: -height * 0.02,
          child: Container(
            decoration: const BoxDecoration(
                color: appColor,
                shape: BoxShape.circle
            ),
            child: const Icon(
              Icons.add,
              color: Colors.black,
              size: 35,
            ),
          ),
        ),
      ],
    ),
  );
}

Widget sliderWidget() {
  return GetX<DashBoardController>(builder: (c) {
    return Column(
      children: [
        CarouselSlider(
            items: c.sliderList,
            options: CarouselOptions(
              autoPlay: true,
              aspectRatio: 16 / 9,
              autoPlayCurve: Curves.fastOutSlowIn,
              enableInfiniteScroll: true,
              autoPlayAnimationDuration: const Duration(milliseconds: 1500),
              viewportFraction: 1,
              onPageChanged: (index, reason) {
                c.sliderIndex.value = index;
              },
            )),
        Padding(
          padding: const EdgeInsets.only(top: 10.0, bottom: 40),
          child: CarouselIndicator(
            count: 4,
            index: c.sliderIndex.value,
            height: 5,
            color: Colors.grey,
            activeColor: appColor,
          ),
        ),
      ],
    );
  });
}

Widget languageWidget() {
  const BorderSide border = BorderSide(color: appColor, width: 5);

  return GetBuilder<DashBoardController>(builder: (controller) {
    return Container(
      width: width,
      height: height * 0.1,
      decoration: BoxDecoration(
          gradient: gradient,
          border: const Border(top: border, bottom: border)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            width: width * 0.6,
            child: ListView.builder(
              itemCount: 4,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                return InkWell(
                  onTap: () {
                    Get.bottomSheet(
                        setLanguageSheet(controller),
                        backgroundColor: Colors.transparent);
                  },
                  child: Center(
                      child: Text('${controller.languageList[index]}, ',
                          style: TextStyle(
                              color: appColor,
                              decoration: index == 0
                                  ? TextDecoration.underline
                                  : TextDecoration.none,
                              decorationColor: appColor,
                              fontWeight: index == 0
                                  ? FontWeight.w800
                                  : FontWeight.w400))),
                );
              },
            ),
          ),
          const Center(
            child: Text('...', style: TextStyle(color: appColor)),
          ),
        ],
      ),
    );
  });
}

Widget setLanguageSheet(DashBoardController controller) {
  return Container(
    height: height,
    margin: const EdgeInsets.symmetric(horizontal: 30),
    padding: const EdgeInsets.all(20),
    decoration: BoxDecoration(
        color: const Color(0xff01145a),
        borderRadius: const BorderRadius.all(Radius.circular(12)),
        border: Border.all(color: appColor)),
    child: Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Text(
              'Select language',
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w700,
                  fontSize: 16),
            ),
            IconButton(
                onPressed: () {
                  Get.back();
                },
                icon: const Icon(Icons.close,color: Colors.white,))
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        SizedBox(
          height: height * 0.4,
          child: ListView(
            children: controller.languageList
                .map((e) => Container(
                decoration: BoxDecoration(
                    border: Border.all(color: appColor)),
                child: Center(
                  child: InkWell(
                    onTap: () {
                      controller.languageList.remove(e);
                      controller.languageList.insert(0, e);
                      controller.update();
                      Get.back();
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(e,
                          style: const TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              color: appColor)),
                    ),
                  ),
                ))).toList(),
          ),
        ),
      ],
    ),
  );
}

Widget timerWidget() {
  const Color appRed = Color(0xff640404);

  return GetBuilder<DashBoardController>(
      builder: (controller) {
        return Stack(
          clipBehavior: Clip.none,
          alignment: Alignment.topCenter,
          children: [
            Container(
              height: height * 0.14,
              width: width * .8,
              decoration: BoxDecoration(
                color: appColor,
                borderRadius: const BorderRadius.all(Radius.circular(12)),
                border: Border.all(
                    color: appRed,
                    width: 8
                ),
              ),
              child: Center(
                child: StreamBuilder(
                  stream: controller.counter(),
                  builder: (context, AsyncSnapshot<int> snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return const CircularProgressIndicator();
                    }
                    return Text(
                      "₹${snapshot.data!.toString()}",
                      style: TextStyle(fontSize: height * 0.050, fontWeight: FontWeight.w800, color: appRed, letterSpacing: width * 0.05),
                    );
                  },
                ),
              ),
            ),
            Positioned(
                bottom: height * 0.11,
                child: textWithStroke(text: 'JACKPOT', strokeColor: appRed, textColor: appColor))
          ],
        );
      }
  );
}

Widget userList() {
  const Color appRed = Color(0xff640404);

  return GetBuilder<DashBoardController>(
      builder: (controller) {
        return Container(
          height: height * 0.12,
          width: width * .9,
          padding: EdgeInsets.symmetric(horizontal: width*0.06, vertical: height*0.02),
          decoration: BoxDecoration(
            gradient: gradient,
            borderRadius: const BorderRadius.all(Radius.circular(12)),
            border: Border.all(
                color: appColor,
                width: 2
            ),
          ),
          child: Center(
            child: StreamBuilder(
              stream: controller.userShuffle(),
              builder: (context, AsyncSnapshot<List<String>> snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const CircularProgressIndicator();
                }
                return Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        userTile(color: appColor, userName: snapshot.data![0]),
                        const Spacer(),
                        userTile(color: appRed, userName: snapshot.data![1]),
                      ],
                    ),
                    const Spacer(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        userTile(color: appRed, userName: snapshot.data![2]),
                        const Spacer(),
                        userTile(color: appColor, userName: snapshot.data![3]),
                      ],
                    ),
                  ],
                );
              },
            ),
          ),
        );
      }
  );
}

Widget userTile({required Color color, required String userName}) {
  return Row(
    children: [
      Container(
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(color: color, width: 2)
          ),
          child: Icon(Icons.account_circle,size: height * 0.025,color: Colors.white)),
      const SizedBox(width: 5),
      Text(userName,
          style: TextStyle(fontSize: height * 0.018,
              fontWeight: FontWeight.w400,
              color: Colors.white)),
    ],
  );
}

Widget gamesWidget() {
  return GetBuilder<DashBoardController>(
      builder: (c) {
        return Column(
          children: [
            Text('GAMES',style: TextStyle(
                color: appColor,
                fontSize: height*.025,
                fontWeight: FontWeight.w700
            ),),
            Container(height: 3,width: width*0.3,color: appColor,margin: const EdgeInsets.only(bottom: 20, top: 1)),
            Row(
              children: [
                Padding(
                  padding: EdgeInsets.only(left: width*0.05,
                      bottom: 5),
                  child: Text('Most popular(6)',style: TextStyle(
                      color: Colors.white,
                      fontSize: height*.025,
                      fontWeight: FontWeight.w700
                  ),),
                ),
                const Spacer(),
                GestureDetector(
                    onTap: () {
                      c.isMoreClicked.value = !c.isMoreClicked.value;
                      c.update();
                    },
                    child: Container(
                        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                        margin: const EdgeInsets.only(right: 14, bottom: 10),
                        decoration: const BoxDecoration(
                            color: appColor,
                            borderRadius: BorderRadius.all(Radius.circular(8))
                        ),
                        child: Center(
                            child: Text('Show More',style: TextStyle(
                                color: Colors.white,
                                fontSize: height*.018,
                                fontWeight: FontWeight.w400
                            )))))
              ],
            ),
            //Games Grid:-
            Container(
              height: c.isMoreClicked.value? height*.76 : height*.5,
              width: width,
              margin: EdgeInsets.symmetric(horizontal: width*0.04),
              child: GridView.builder(
                  physics: const NeverScrollableScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2,
                      mainAxisSpacing: height*0.03,
                      crossAxisSpacing: height*0.02,
                      childAspectRatio: 1.0),
                  itemCount: c.isMoreClicked.value? c.gamesList.length : 4,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                        decoration: BoxDecoration(
                            borderRadius: const BorderRadius.all(Radius.circular(8)),
                            color: Colors.white38,
                            border: Border.all(color: appColor, width: 1.5)
                        ),
                        child: Column(
                          children: [
                            Image.asset(c.gamesList[index],
                                height: height*.18,
                                width: height*.15,
                                fit: BoxFit.fill),
                            Container(
                              color: Colors.purple.shade800,
                              child: const Center(
                                child: Text('Min:₹10 | Max:100k',
                                  style: TextStyle(
                                      fontSize: 12,
                                      height: 2,
                                      color: Colors.white
                                  ),),
                              ),
                            )
                          ],
                        ));
                  }
              ),
            )
          ],
        );
      }
  );
}

Widget textWithStroke({required String text, double strokeWidth = 5,
  required Color textColor, required Color strokeColor}) {
  return Stack(
    children: <Widget>[
      Text(
        text,
        style: TextStyle(
          fontSize: height * 0.040,
          fontStyle: FontStyle.italic,
          foreground: Paint()
            ..style = PaintingStyle.stroke
            ..strokeWidth = strokeWidth
            ..color = strokeColor,
        ),
      ),
      Text(text, style: TextStyle(fontSize: height * 0.040,
          fontStyle: FontStyle.italic,
          color: textColor)),
    ],
  );
}