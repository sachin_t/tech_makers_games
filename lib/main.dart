import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tech_makers/dashboard_controller.dart';
import 'package:tech_makers/dashboard_view.dart';

void main() {
  Get.put(DashBoardController());
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Technology makers test app',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const DashBoardView(),
    );
  }
}
